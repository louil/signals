
#ifndef signaler
 #define signaler

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <getopt.h>

#define MAX UINT_MAX

int get_args(int argc, char *argv[]);
int loop();
void handler(int sig, siginfo_t *siginfo, void *context);
bool is_prime(int var);

//Global variables to be used throughout the program
int next = 1;
int num = 2;
int to_the_next = 0;
long long maximum = MAX;	

//Function that uses optarg to differentiate between the optional command-line flags for -s, -r, and -e
int get_args(int argc, char *argv[]){
	int c = -1;

	while ((c = getopt(argc, argv, "r:s:e:")) != -1) {
		char *endptr = NULL;
		switch (c) {
			case 's':
				if (num != 2){
					fprintf(stderr, "-s option can only be used by itself.\n");
					return -1;
				}
				num = strtol(optarg, &endptr, 10);
				if (endptr == optarg) {
					fprintf(stderr,"No end number.\n");
					return(-1);
				}
				num+=1;
				break;
			case 'r':
				next *= -1;
				if (num != 2){
					fprintf(stderr, "-r option can only be used by itself.\n");
					return -1;
				}
				num = strtol(optarg, &endptr, 10);
				if (endptr == optarg) {
					fprintf(stderr,"No end number.\n");
					return(-1);
				}
				break;
			case 'e':
				maximum = strtol(optarg, &endptr, 10);
				if (endptr == optarg) {
					fprintf(stderr,"No end number.\n");
					return(-1);
				}
				break;
			default:
				fprintf(stderr, "Non-valid flag entered.\n");
				return -1;
		}
	}

	return 0;
}

//Function derived from Samuels that loops through calling the is_prime function to find out if a number is prime
//and if so then prints it to the screen
int loop(){

	bool prime = false;

	while (num < maximum && num > 1){
		while (prime == false){
			if ((prime = is_prime(num))){
				if (to_the_next){
					to_the_next = 0;
				}
				else{
					printf("%d\n", num);
				}
				prime = false;
				break;
			}
			else{

			}
			num += next;
			if (num >= maximum){
				return 0;
			}
		}
		sleep(1);
		num += next;
	}

	return 0;
}

//Function derived from https://www.linuxprogrammingblog.com/code-examples/sigaction
void handler(int sig, siginfo_t *siginfo, void *context){

	//context is not being used but was part of the information from the link
	(void)context;	

	printf("PID: %ld, UID: %ld\n", (long)siginfo->si_pid, (long)siginfo->si_uid);

	switch (sig){
		case(1):
			num = 2;
			break;
		case(10):
			to_the_next = 1;
			break;
		case(12):
			next *= -1;
			break;
		default:
			break;
	}
}

//Function derived from statments made from http://stackoverflow.com/questions/5811151/why-do-we-check-upto-the-square-root-of-a-prime-number-to-determine-if-it-is-pri 
//that checks to see if the number passed is a prime number
bool is_prime(int var){

	if (var == 2){
		return true;
	}	
	else if (var % 2 == 0){
		return false;
	}

	int num = (int)sqrt(var) + 1;

	for (int i = 3; i <= num; i+=2){
		if (var % i == 0){
			return false;
		}
	}
	return true;
}

#endif
