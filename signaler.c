
#include "signaler.h"

int main(int argc, char *argv[]){

	//srtucture format and sigaction usage derived from https://gist.github.com/aspyct/3462238
	struct sigaction act;
 
	memset (&act, '\0', sizeof(act));
 
	/* Use the sa_sigaction field because the handles has two additional parameters, from https://gist.github.com/aspyct/3462238*/
	act.sa_sigaction = &handler;
 
	/* The SA_SIGINFO flag tells sigaction() to use the sa_sigaction field, not sa_handler. From https://gist.github.com/aspyct/3462238*/
	act.sa_flags = SA_SIGINFO;

	//Block of if statements to check for signals
	if (sigaction(SIGHUP, &act, NULL) < 0){
		perror("SIGHUP not handled.\n");
		return -1;
	}
	if (sigaction(SIGUSR1, &act, NULL) == -1){
		perror("Signal not handled properly.\n");
		return -1;
	}
	if (sigaction(SIGUSR2, &act, NULL) == -1){
		perror("Signal not handled properly.\n");
		return -1;
	}

	//If statement that calls to the get_args function that then uses optarg to check for flags
	if (get_args(argc, argv) == -1){
		fprintf(stderr, "Non-valid flag entered.\n");
		return -1;
	}

	//Main fuction that loops through and checks if numbers are prime and if so then prints to screen
	loop();

	return 0;
}
